import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';


import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'Attente.dart';
import 'inscription_page.dart';
import 'package:flutter_svg/svg.dart';
import 'selfie_page.dart';
import 'package:mime/mime.dart';
class MyHomePage extends StatefulWidget {
  MyHomePage({ required this.title, required this.imagePath,
      required this.lastname,
      required this.firstname,
      required this.email,
      required this.phone,
      required this.mdp});

  final String title;
   final File
   imagePath;
  final String firstname;
  final String lastname;
  final String email;
  final String phone;
  final String mdp;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File? imageBytes;

  bool selecimage = false;
  bool selectext = true;


   File? imagePath;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /*appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white.withOpacity(0),
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 30,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          ),*/

        body: Container(
            child: Padding(
      padding: const EdgeInsets.all(30.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // SizedBox(height: 48,),
          Spacer(
            flex: 2,
          ),

          Text("Joindre votre pièce d'identité.\n ",
              style: TextStyle(
                fontSize: 32.0,
                fontWeight: FontWeight.bold,
                letterSpacing: 2.0,
                color: Color(0xFF4361EE),
                fontFamily: 'NexaBold',
              )),
          Text(
              "Complétez votre inscription en toute sécurité avec un scan de votre permis de conduite.\n ",
              style: TextStyle(
                fontSize: 15.0,
                letterSpacing: 2.0,
                color: Colors.grey[400],
                fontFamily: 'NexaThin',
              )),

          Spacer(
            flex: 2,
          ),

          selecimage
              ? SvgPicture.asset('images/ID_Card.svg', width: 230, height: 230)
              : Container(
                  height: (MediaQuery.of(context).size.height) / 2.5,
                  width: (MediaQuery.of(context).size.width),
                  child: ClipOval(
                    child: buildImage(),
                    //borderRadius: BorderRadius.circular((MediaQuery.of(context).size.width) / 4),
                  ),
                ),

          Text('\n'),

          Spacer(
            flex: 1,
          ),

          ElevatedButton(
            style: ElevatedButton.styleFrom(
                primary: Color(0xFF4361EE),
                padding:
                    const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(17.0))),
            onPressed: () {
              if (selectext == true) {
                pickImage();
                selectext = false;
              } else {
                signup(context);
              }
            },
            child: selectext
                ? Text(
                    "Joindre une photo",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 19.0,
                      letterSpacing: 2.0,
                      color: Color(0xFFFFFFFF),
                      fontFamily: 'NexaBold',
                    ),
                  )
                : Text(
                    "Terminer",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 19.0,
                      letterSpacing: 2.0,
                      color: Color(0xFFFFFFFF),
                      fontFamily: 'NexaBold',
                    ),
                  ),
          ),

          Expanded(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  // Toggle light when tapped.
                  selecimage = !selecimage;
                  selectext = !selectext;
                });
              },
            ),
          ),
        ],
      ),
    )));
  }

  Future pickImage() async {
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);

    setState(() {
      imageBytes = File(image!.path);
    });
  }

  Widget buildImage() {
    return imageBytes != null
       ?  Image.file(imageBytes!)
        : Container(
            child:
                SvgPicture.asset('images/ID_Card.svg', width: 230, height: 230),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              //more than 50% of width makes circle
            ),
          );
  }
  // recuperation de l'image
late String location2;
  Future<Map<String, dynamic>?> _uploadImage2() async {
    // Récupérée de la caméra, doit être de type file
    var image = imageBytes;
    var request = http.MultipartRequest(
      'POST', Uri.parse( 'https://wyerkn74ia.execute-api.eu-west-3.amazonaws.com/upload'),
    );

    Map<String,String> headers={
      "Content-type": "multipart/form-data" // Cette entête http est nécessaire
    };
    request.headers.addAll(headers);

// Ajouter le fichier
    request.files.add(
      http.MultipartFile(
        'file',
        image!.readAsBytes().asStream(),
        image.lengthSync(),
        filename: basename(image.path),
        contentType: MediaType('image','jpeg'),
      ),
    );






    try {
      final res = await request.send();
      print(res.statusCode);


      final response = await http.Response.fromStream(res);
     
      final Map<String, dynamic> Data = json.decode(response.body);
      location2= Data['location'];
      //print("this is permis location : ${location2}");


      print(response.statusCode);
      if (response.statusCode == 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);

      // _resetState();
      return responseData;
    } catch (e) {
      print(e);
      return null;
    }
  }
  late String location;
  Future<Map<String, dynamic>?> _uploadImage() async {
     var image = widget.imagePath;// Récupérée de la caméra, doit être de type file

// Préparer la requête
    var request = http.MultipartRequest(
      'POST', Uri.parse( 'https://wyerkn74ia.execute-api.eu-west-3.amazonaws.com/upload'),
    );

    Map<String,String> headers={
      "Content-type": "multipart/form-data" // Cette entête http est nécessaire
    };
    request.headers.addAll(headers);

// Ajouter le fichier
    request.files.add(
      http.MultipartFile(
        'file',
        image.readAsBytes().asStream(),
        image.lengthSync(),
        filename: basename(image.path),
        contentType: MediaType('image','jpeg'),
      ),
    );






    try {
      final res = await request.send();
      print(res.statusCode);


      final response = await http.Response.fromStream(res);
      //print(response.body);
      final Map<String, dynamic> Data = json.decode(response.body);
       location= Data['location'];
      //print("this is selfie location : ${location}");


      print(response.statusCode);
      if (response.statusCode == 200) {
        return null;
      }
      final Map<String, dynamic> responseData = json.decode(response.body);

     // _resetState();
      return responseData;
    } catch (e) {
      print(e);
      return null;
    }
  }


  Future<void> signup(BuildContext context) async {
   // print(widget.lastname);
   // print(widget.firstname);
   // print(widget.email);
   // print(widget.mdp);
   // print(widget.phone);


    if (widget.lastname.isNotEmpty &&
        widget.firstname.isNotEmpty &&
        widget.email.isNotEmpty &&
        widget.phone.isNotEmpty &&
        widget.mdp.isNotEmpty) {

      final Map<String, dynamic>? ress = await _uploadImage();
      if (ress == null) {

        print('User selfie uploaded successfully');
        final Map<String, dynamic>? ress2 = await _uploadImage2();
        if (ress2 == null) {

          print('User permis uploaded successfully');
        var response = await http.post(
            Uri.parse(
                'https://wyerkn74ia.execute-api.eu-west-3.amazonaws.com/signup/locataire'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              "email": widget.email,
              "mdp": widget.mdp,
              "prenom":  widget.firstname,
              "nom": widget.lastname,
              "num_tel": widget.phone,
              "adresse_locataire": "vhvvvhvhvhunnnnnnnnnn",
              "photo":location,
              "piece_identite":location2,
            }));

        print('Response status: ${response.body}');
        if (response.statusCode == 200) {
         // print("hello world");
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => WaitingPage()));
        } else {
          if (response.statusCode == 401) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text("Details manquants")));
          } else {
            if (response.statusCode == 405) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text("User Already exists")));
            } else {
              if (response.statusCode == 500) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text("Erreur Serveur")));
              }
            }
          }
        }

      }
        else {
          print('Veuillez ajouter votre photo de permis SVP ');
        }
      }
     else {
      print('Veuillez prendre un selfie SVP ');
    }


    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("veuillez remplir les champs")));
    }
  }



}



class MyCircleClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, 100, 200);
  }

  @override
  bool shouldReclip(MyCircleClipper oldClipper) {
    return false;
  }
}
