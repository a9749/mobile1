// To parse this JSON data, do
//
//     final bienModel = bienModelFromJson(jsonString);
import 'package:flutter/material.dart';
//import 'dart:convert';

//import 'package:mongo_dart/mongo_dart.dart';

//BienModel bienModelFromJson(String str) => BienModel.fromJson(json.decode(str));

//String bienModelToJson(BienModel data) => json.encode(data.toJson());

class FactureUser {
  FactureUser({
    required this.id,
    required this.prix,
    required this.depart,
    required this.arrivee,
    required this.date,
    
  });

  String id;
  double prix;
  String depart;
  String arrivee;
  DateTime date;
  
}
  final List<FactureUser> mesFactures = [
  FactureUser(
    id: '000001',
    prix: 5000 ,
    depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
    arrivee:'Ain Taya, Algerie',
    date: DateTime.now(),
  ),
    FactureUser(
      id: '000001',
      prix: 5000 ,
      depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
      arrivee:'Ain Taya, Algerie',
      date: DateTime.now(),
      
    ),
    FactureUser(
      id: '000001',
      prix: 5000 ,
      depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
      arrivee:'Ain Taya, Algerie',
      date: DateTime.now(),
      
    ),
    FactureUser(
      id: '000001',
      prix: 5000 ,
      depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
      arrivee:'Ain Taya, Algerie',
      date: DateTime.now(),
     
    ),
    FactureUser(
      id: '000001',
      prix: 5000 ,
      depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
      arrivee:'Ain Taya, Algerie',
      date: DateTime.now(),
     
    ),
    FactureUser(
      id: '000001',
      prix: 5000 ,
      depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
      arrivee:'Ain Taya, Algerie',
      date: DateTime.now(),
      
    ),
    FactureUser(
      id: '000001',
      prix: 5000 ,
      depart: 'Hopital Salim Zemirli,El Harrach, Algerie',
      arrivee:'Ain Taya, Algerie',
      date: DateTime.now(),
    
    ),
  ];
/*
  factory BienModel.fromJson(Map<String, dynamic> json) => BienModel(
    id: json["_id"],
    code: json["code"],
    intitule: json["intitule"],
    unite: json["unite"],
    emplacement: json["emplacement"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id.toJson(),
    "code": code,
    "intitule": intitule,
    "unite": unite,
    "emplacement": emplacement,
  };
}*/


