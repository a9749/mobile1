import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'cars_types.dart';
class carType {
  double unitPrice;
  
  t type;
  carType(
      {required this.unitPrice,
      required this.type,});
}
final List<carType> carTypes = [
  carType(
      type: t.breaks,
    unitPrice: 140,
  ),
  carType(
      type: t.limousines,
    unitPrice: 130,
  ),carType(
      type: t.cabriolet,
    unitPrice: 120,
  ),carType(
      type: t.targa,
    unitPrice: 110,
  ),carType(
      type: t.coupes,
    unitPrice: 100,
  ),];
    
  