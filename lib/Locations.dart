import 'package:AutoRun/main.dart';
import 'package:flutter/material.dart';
import 'icons/menu_icons_icons.dart';

class Location {
 
  String title;
  String detail;
  String depart;
  String arrivee;
  String date;
 

  Location({ this.title = 'hah', this.detail='', this.depart="", this.arrivee="", this.date=""});
}

final location = [
  Location(
    
    title: 'Audi R8',
    detail: 'Voir détails',
    depart: "Av. Colonel Mellah Ali, Algiers 16000",
    arrivee: 'Oued Smar, Algiers 16000',
    date: "Aujourd’hui , à 12:46",
   
  ),
  Location(
    
    title: 'Audi R8',
    detail: 'Voir détails',
    depart: "Av. Colonel Mellah Ali, Algiers 16000",
    arrivee: 'Oued Smar, Algiers 16000',
    date: "Hier , à 12:46"
  ),
  Location(
   
    title: 'Audi R8',
    detail: 'Voir détails',
    depart: "Av. Colonel Mellah Ali, Algiers 16000",
    arrivee: 'Oued Smar, Algiers 16000',
    date: "31 mars , à 12:46"
    
  ),
  Location(
    
    title: 'Audi R8',
    detail: 'Voir détails',
    depart: "Av. Colonel Mellah Ali, Algiers 16000",
    arrivee: 'Oued Smar, Algiers 16000',
    date: "21 mars , à 12:46"
  ),
  Location(
    
    title: 'Audi R8',
    detail: 'Voir détails',
    depart: "Av. Colonel Mellah Ali, Algiers 16000",
    arrivee: 'Oued Smar, Algiers 16000',
    date: "11 mars , à 12:46"
  ),
   Location(
    
    title: 'Audi R8',
    detail: 'Voir détails',
    depart: "Av. Colonel Mellah Ali, Algiers 16000",
    arrivee: 'Oued Smar, Algiers 16000',
    date: "9  mars , à 12:46"
  ),
];
