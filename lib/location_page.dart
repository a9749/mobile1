import 'package:AutoRun/Locations.dart';
import 'package:AutoRun/login_page.dart';
import 'package:AutoRun/welcome_page.dart';
import 'package:flutter/material.dart';
import 'main.dart';

import 'dart:ui';

import 'package:AutoRun/main.dart';
import 'package:flutter/material.dart';
import 'Locations.dart';

class ListPage extends StatefulWidget {
  const ListPage({Key? key}) : super(key: key);

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        
            leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: d_blue,
            size: 18,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
            elevation: 0,
            backgroundColor: Color.fromARGB(255, 247, 245, 245),
            title: Text("Mes Locations",
            style: TextStyle(
                    fontFamily: 'NexaBold',
                    color: d_blue,
                    fontSize: 22,
                  )),
           

            
      ),
      body: Column(
        
        mainAxisSize: MainAxisSize.min,
        children: [
         
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListView.builder(
                itemCount: location.length,
                itemBuilder: (BuildContext context, int index) {
                  final Location loc = location[index];
                  return Container(
                    margin: const EdgeInsets.symmetric(vertical: 15.0),
                    height: MediaQuery.of(context).size.height * .30,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white.withOpacity(.5),
                          spreadRadius: 2,
                          blurRadius: 4,
                          offset: Offset(10, 5),
                        )
                      ],
                    ),
                    child: Stack(
                      fit: StackFit.expand,
                      children: [
                        ListItemContainer(loc: loc),
                        Align(
                          alignment: Alignment.centerRight,
                          child: ClipPath(
                            clipper: ArcClipper(),
                            child: Container(
                              color: Colors.white.withOpacity(.3),
                              width: MediaQuery.of(context).size.width * .28,
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ArcClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path = Path();

    path.moveTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.lineTo(size.width - 30, 0);
    path.quadraticBezierTo(3, size.height * .25, 0, size.height);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ListItemContainer extends StatelessWidget {
  const ListItemContainer({
    Key? key,
    required this.loc,
  }) : super(key: key);

  final Location loc;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 15.0,
        horizontal: 20.0,
      ),
      child: Row(
        children: [
         
          const SizedBox(width: 20),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  loc.title,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                
                const SizedBox(height: 10),
                Text(
                   loc.depart,
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                 const SizedBox(height: 10),
                Text(
                   loc.arrivee,
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                   loc.date,
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w500,
                  ),
                ),
               const Spacer(),
                
              
               Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                  loc.detail,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: d_blue,
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                  ),
                ),
              
              
              IconButton(
                icon: const Icon(Icons.arrow_forward_ios),
                iconSize: 15.0,
                color: d_blue, 
                onPressed: () { Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => WelcomePage(),
                      ),
                    ); },
                
                
              )
              
            ],
          ),
              ],
            ),
          ),
        
          
        ],
      ),
    );
  }
}





class TabItem extends StatelessWidget {
  const TabItem({
    Key? key,
    required this.title,
    required this.active,
  }) : super(key: key);

  final String title;
  final bool active;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      child: Transform(
        transform: Matrix4.identity()..scale(active ? 1.5 : 1.0),
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
          maxLines: 1,
        ),
      ),
    );
  }
}