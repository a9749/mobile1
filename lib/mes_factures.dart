import 'package:flutter/material.dart';
import 'factures_model.dart';
import 'main.dart';
import 'package:AutoRun/welcome_page.dart';
class factures extends StatefulWidget {
  @override
  _facturesState createState() => _facturesState();
}

class _facturesState extends State<factures> {
  late PageController _pageController;
  int prevPage = 0;
  @override
  void initState() {

    _pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(_onScroll);
  }

  void _onScroll() {
    if (_pageController.page!.toInt() != prevPage) {
      prevPage = _pageController.page!.toInt();

    }
  }
    final icons = [Icons.ac_unit, Icons.access_alarm, Icons.access_time,Icons.ac_unit, Icons.access_alarm, Icons.access_time,Icons.ac_unit];

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: 
                Text(
                  "      Mes Factures   ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color:d_blue,
                      letterSpacing: 1),
                ),
                
        elevation: 0,
        backgroundColor: Colors.white.withOpacity(0),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
            size: 30,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
        body:ListView.separated(
      separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: mesFactures.length,
        itemBuilder: (context, index) {
          return Card(
              child: Container(
                height : 180,
               
                          margin: EdgeInsets.only(left: 15,top: 20),
                child:Column(
                  children:[
                    Row(
            

                      children:[
                         Text( "ID:  " + mesFactures[index].id,style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontFamily: 'Nunito',
                                                          fontSize: 18,
                                                          color: Colors.black)),
                         SizedBox(width: 80),
                         Text(mesFactures[index].prix.toString() +" DA",style: TextStyle(
                                                          
                                                              
                                                          fontFamily: 'Nunito',
                                                          fontSize: 19,
                                                          color: d_blue)),
                                                          SizedBox(width: 15),
                           IconButton(
                icon: const Icon(Icons.arrow_forward_ios),
                iconSize: 15.0,
                color: d_blue, 
                onPressed: () { Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => WelcomePage(),
                      ),
                    ); },
                
                
              ),
                      ]
                    ),
                     SizedBox(height: 18),
                    Row(
            

                      children:[
                        Container(
                              height: 70.0,
                              width: 47.0,
                              decoration: BoxDecoration(
                                 
                                  image: DecorationImage(
                                      image: AssetImage("images/distance.png"),
                                      fit: BoxFit.cover))),
                        
                        Column(
                             children:[
                               Text(mesFactures[index].depart,style: TextStyle(
                                                          
                                                              
                                                          fontFamily: 'Nunito',
                                                          fontSize: 16,
                                                          color: Colors.grey)),
                                SizedBox(height: 10),
                               Text(mesFactures[index].arrivee,style: TextStyle(
                                                          
                                                              
                                                          fontFamily: 'Nunito',
                                                          fontSize: 16,
                                                          color: Colors.grey)),

                             ]
                        )
                         
                      ]
                    ),

                   SizedBox(height: 20),
                  
                   Text("Date:  "+mesFactures[index].date.toString(),style: TextStyle(
                                                          
                                                              
                                                          fontFamily: 'Nunito',
                                                          fontSize: 15,
                                                          color: Colors.black)),
                
                  
                  ]
                )));
        }));
  }
   
  


}
