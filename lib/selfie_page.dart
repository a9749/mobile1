import 'dart:async';
import 'dart:io';

import 'main.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'joindre_permis.dart';


//import 'package:http/http.dart'as http;
Future<Widget> Picture(String lastname,String firstname,String email,
  String  phone,String mdp) async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  final firstCamera = cameras[1];

  return TakePictureScreen(
    camera: firstCamera,
    lastname: lastname,
    firstname: firstname,
    email: email,
    phone: phone,
    mdp: mdp,
  );
}

class TakePictureScreen extends StatefulWidget {
  TakePictureScreen({

    required this.camera,
      required this.lastname,
      required this.firstname,
      required this.email,
      required this.phone,
      required this.mdp
  }) ;

  final CameraDescription camera;
  final String firstname;
  final String lastname;
  final String email;
  final String phone;
  final String mdp;
  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  
  late CameraController _controller;

  late String firstname;
  late String lastname;
  late String email;
  late String phone;
  late String mdp;
  late Future<void> _initializeControllerFuture;
  int i = 0;
  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
      
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Prendre une photo'),
        backgroundColor: d_blue,
      ),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          try {
            await _initializeControllerFuture;
            var image = await _controller.takePicture()  ;
           File selfie=File(image.path);

            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(
                  imagePath: selfie,
                  lastname: widget.lastname,
                  firstname: widget.firstname,
                  email: widget.email,
                  phone: widget.phone,
                  mdp: widget.mdp,
              
                ),
              ),
            );
          } catch (e) {
            print(e);
          }
        },
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  File imagePath;
  DisplayPictureScreen(
      {
      required this.imagePath,
       required this.firstname,
      required this.lastname,
      required this.email,
      required this.phone,
      required this.mdp});

  final String firstname;
  final String lastname;
  final String email;
  final String phone;
  final String mdp;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Afficher l'image"),
          backgroundColor: d_blue,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.arrow_forward),
              color: Colors.white,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyHomePage(
                      title: '',
                      lastname: lastname,
                      firstname:firstname,
                      email: email,
                      phone: phone,
                      mdp: mdp,
                      imagePath:imagePath,
                    ),
                  ),
                );
              },
            ),
          ]),
      body: Image.file(imagePath),
    );
  }
}
