import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'cars_types.dart';
import 'types_prices.dart';
class Vehicule {
  String matricule;
  String marque;
  String modele;
  String couleur;
  t type;
  String charge;
  double prix;
  double distance;
  String location;
  String image;
  LatLng locationCoords;
  String temps;

  Vehicule(
      {required this.matricule,
      required this.marque,
      required this.modele,
      required this.couleur,
      required this.type,
      required this.charge,
      required this.prix,
      required this.distance,
      required this.location,
      required this.image,
      required this.locationCoords,
      required this.temps});
}

final List<Vehicule> vehicules = [
  Vehicule(
    matricule: '123456 00 16',
    marque: 'Audi',
    modele: 'R8',
    couleur: 'Grise',
    type: carTypes[0].type,
    charge: "98%",
    prix: carTypes[0].unitPrice,
    distance: 88,
    temps: "01h 12min",
    location: "Av.Colonel Mellah Ali,Algier",
    locationCoords: LatLng(36.7386643, 3.34255),
    image: 'images/Audi.png',
  ),
  Vehicule(
      matricule: '123456 00 16',
      marque: 'Audi',
      modele: 'R8',
      couleur: 'Grise',
      type:carTypes[1].type,
      charge: "98%",
      prix: carTypes[1].unitPrice,
      distance: 50,
      temps: "2h 35min",
      location: "Av.C.Amirouche,El harrach",
      locationCoords: LatLng(36.73402859153134, 3.0925820867497014),
      image: 'images/Audi.png'),
  Vehicule(
      matricule: '123456 00 16',
      marque: 'Audi',
      temps: "01h 48min",
      modele: 'R8',
      couleur: 'Grise',
      type: carTypes[2].type,
      charge: "98%",
      prix: carTypes[2].unitPrice,
      distance: 75.5,
      location: "Av.Hassiba ben bouali,Algiers",
      locationCoords: LatLng(36.72219640576765, 3.183905938245022),
      image: 'images/Audi.png'),
  Vehicule(
      matricule: '123456 00 16',
      marque: 'Audi',
      modele: 'R8',
      temps: "2h 57min",
      couleur: 'Grise',
      type:carTypes[3].type,
      charge: "98%",
      prix: carTypes[3].unitPrice,
      distance: 60.3,
      location: "Av.Tamaris,Dergana",
      locationCoords: LatLng(36.69742546502124, 3.075415949250581),
      image: 'images/Audi.png'),
  Vehicule(
      matricule: '123456 00 16',
      marque: 'Audi',
      modele: 'R8',
      couleur: 'Grise',
      type: carTypes[4].type,
      charge: "98%",
      prix: carTypes[4].unitPrice,
      temps: "03h 12min",
      distance: 170,
      location: "Av.1er Novembre,Reghaia",
      locationCoords: LatLng(36.696324348991226, 3.129660943747801),
      image: 'images/Audi.png'),
];
