import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'Location_service.dart';
import 'cars_model.dart';
import 'menu_page.dart';
import 'package:flutter_google_places_hoc081098/flutter_google_places_hoc081098.dart';
import 'package:google_api_headers/google_api_headers.dart';

import 'package:google_maps_webservice/places.dart';
import 'package:slidable_button/slidable_button.dart';
import 'package:geolocator/geolocator.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'main.dart';

import 'inside_car.dart';

class PageAcceuil extends StatefulWidget {
  @override
  _PageAcceuilState createState() => _PageAcceuilState();
}

class _PageAcceuilState extends State<PageAcceuil> {
  String googleApikey = "AIzaSyCNdS-eHQeAsWyQ6xIEwROKmkgaA7zm6a4";
  String location = "Origine";
 var geometry;
  String location2 = "Destination";
  TextEditingController _searchController = TextEditingController();
  TextEditingController _Origincontroller = TextEditingController();
  TextEditingController _Destinationcontroller = TextEditingController();
  late GoogleMapController _controller;

  List<Marker> allMarkers = [];

  late PageController _pageController;

  int prevPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    vehicules.forEach((element) {
      allMarkers.add(Marker(
          markerId: MarkerId(element.marque),
          draggable: false,
          onTap: () {},
          infoWindow:
              InfoWindow(title: element.marque, snippet: element.location),
          position: element.locationCoords));
    });
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(_onScroll);
  }

  void _onScroll() {
    if (_pageController.page!.toInt() != prevPage) {
      prevPage = _pageController.page!.toInt();
      moveCamera();
    }
  }

  _coffeeShopList(index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (context, widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = (_pageController.page! - index);
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 120.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: InkWell(
          onTap: () {
            showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                builder: (builder) {
                  return Expanded(
                      flex: 2,
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          //color: color,
                          color: d_blue,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              topLeft: Radius.circular(20)),
                          boxShadow: [
                            BoxShadow(
                                color: d_blue, spreadRadius: 0, blurRadius: 10),
                          ],
                        ),
                        child: Column(
                          children: [
                            Expanded(
                                flex: 1,
                                child: Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(20),
                                          topLeft: Radius.circular(20)),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(top: 30),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                      vehicules[index].marque +
                                                          " " +
                                                          vehicules[index]
                                                              .modele,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontFamily: 'Nunito',
                                                          fontSize: 25,
                                                          color: Colors.white)),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Text(vehicules[index].charge,
                                                      style: TextStyle(
                                                          fontFamily: 'Nunito',
                                                          fontSize: 15,
                                                          color: Colors.white))
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        Image.asset(
                                          "images/Audi.png",
                                          width: 150,
                                        )
                                      ],
                                    ))),
                            Expanded(
                                flex: 5,
                                child: Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(20),
                                          topLeft: Radius.circular(20)),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black38,
                                            spreadRadius: 0,
                                            blurRadius: 10),
                                      ],
                                    ),
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          left: 40, top: 20, right: 20),
                                      child: Column(children: [
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text("Localisation",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily: 'Nunito',
                                                    fontSize: 15,
                                                    color: Colors.black)),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Icon(
                                                  MdiIcons.send,
                                                  //color: color,
                                                  size: 10,
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                    vehicules[index]
                                                            .distance
                                                            .toString() +
                                                        " mètres",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'Nunito',
                                                        fontSize: 10,
                                                        color: Colors.black))
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.black),
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            child: Container(
                                              margin: EdgeInsets.all(10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Icon(MdiIcons.marker
                                                      //color: color,
                                                      ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Text(
                                                      vehicules[index].location,
                                                      style: TextStyle(
                                                          fontFamily: 'Nunito',
                                                          fontSize: 15,
                                                          color: Colors.grey))
                                                ],
                                              ),
                                            )),
                                        SizedBox(
                                          height: 30,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Text("Caractéristiques",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'Nunito',
                                                        fontSize: 18,
                                                        color: Colors.black)),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.black),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15)),
                                                      child: Container(
                                                        margin:
                                                            EdgeInsets.all(10),
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Text("Marque",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .black)),
                                                            SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                                vehicules[index]
                                                                    .marque,
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .grey))
                                                          ],
                                                        ),
                                                      )),
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.black),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15)),
                                                      child: Container(
                                                        margin:
                                                            EdgeInsets.all(10),
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Text("Modèle",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .black)),
                                                            SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                                vehicules[index]
                                                                    .modele,
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .grey))
                                                          ],
                                                        ),
                                                      )),
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.black),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15)),
                                                      child: Container(
                                                        margin:
                                                            EdgeInsets.all(10),
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Text("Couleur",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .black)),
                                                            SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                                vehicules[index]
                                                                    .couleur,
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .grey))
                                                          ],
                                                        ),
                                                      )),
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.black),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15)),
                                                      child: Container(
                                                        margin:
                                                            EdgeInsets.all(10),
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Text("Type",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .black)),
                                                            SizedBox(
                                                              height: 5,
                                                            ),
                                                            Text(
                                                                vehicules[index]
                                                                    .type.toString(),
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Nunito',
                                                                    fontSize:
                                                                        15,
                                                                    color: Colors
                                                                        .grey))
                                                          ],
                                                        ),
                                                      )),
                                                ]),
                                            Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.black),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                child: Container(
                                                  margin: EdgeInsets.all(10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Text("Matricule",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Nunito',
                                                              fontSize: 15,
                                                              color: Colors
                                                                  .black)),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Text(
                                                          vehicules[index]
                                                              .matricule,
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Nunito',
                                                              fontSize: 15,
                                                              color:
                                                                  Colors.grey))
                                                    ],
                                                  ),
                                                )),
                                            SizedBox(
                                              height: 20,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text("Méthode de paiement",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontFamily: 'Nunito',
                                                        fontSize: 15,
                                                        color: Colors.black)),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.black),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                child: Container(
                                                  margin: EdgeInsets.all(10),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Text(
                                                          "xxxx-xxxx-xxxx-1234",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Nunito',
                                                              fontSize: 15,
                                                              color:
                                                                  Colors.grey)),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Image.asset(
                                                        "images/BARIDIMOB.png",
                                                        width: 50,
                                                        height: 50,
                                                      ),
                                                    ],
                                                  ),
                                                )),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 30,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 3,
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: Colors.black),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                child: Container(
                                                  margin: EdgeInsets.all(10),
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Text(
                                                          vehicules[index]
                                                                  .prix
                                                                  .toString() +
                                                              ' ' +
                                                              'DA',
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Nunito',
                                                              fontSize: 15,
                                                              color: Colors
                                                                  .black)),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Text("par heure",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Nunito',
                                                              fontSize: 15,
                                                              color:
                                                                  Colors.grey))
                                                    ],
                                                  ),
                                                )),
                                            SizedBox(
                                              width: 15,
                                            ),
                                            Container(
                                              alignment: Alignment.bottomLeft,
                                              height: 50,
                                              width: 150,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(15),
                                                  color: d_blue),
                                              child: MaterialButton(
                                                  onPressed: () {
                                                    showModalBottomSheet(
                                                        isScrollControlled:
                                                            true,
                                                        context: context,
                                                        builder: (builder) {
                                                          return Expanded(
                                                              flex: 2,
                                                              child: Container(
                                                                width: double
                                                                    .infinity,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  //color: color,
                                                                  color: d_blue,
                                                                  borderRadius: BorderRadius.only(
                                                                      topRight:
                                                                          Radius.circular(
                                                                              20),
                                                                      topLeft: Radius
                                                                          .circular(
                                                                              20)),
                                                                  boxShadow: [
                                                                    BoxShadow(
                                                                        color:
                                                                            d_blue,
                                                                        spreadRadius:
                                                                            0,
                                                                        blurRadius:
                                                                            10),
                                                                  ],
                                                                ),
                                                                child: Column(
                                                                  children: [
                                                                    Expanded(
                                                                        flex: 1,
                                                                        child: Container(
                                                                            width: double.infinity,
                                                                            decoration: BoxDecoration(
                                                                              borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                                                                            ),
                                                                            child: Row(
                                                                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                              children: [
                                                                                Container(
                                                                                  margin: EdgeInsets.only(top: 30),
                                                                                  child: Column(
                                                                                    children: [
                                                                                      Row(
                                                                                        children: [
                                                                                          Text(vehicules[index].marque + " " + vehicules[index].modele, style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Nunito', fontSize: 25, color: Colors.white)),
                                                                                        ],
                                                                                      ),
                                                                                      Row(
                                                                                        children: [
                                                                                          Text(vehicules[index].charge, style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.white))
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                Image.asset(
                                                                                  "images/Audi.png",
                                                                                  width: 150,
                                                                                )
                                                                              ],
                                                                            ))),
                                                                    Expanded(
                                                                        flex: 5,
                                                                        child: Container(
                                                                            width: double.infinity,
                                                                            decoration: BoxDecoration(
                                                                              color: Colors.white,
                                                                              borderRadius: BorderRadius.only(topRight: Radius.circular(20), topLeft: Radius.circular(20)),
                                                                              boxShadow: [
                                                                                BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
                                                                              ],
                                                                            ),
                                                                            child: Container(
                                                                              margin: EdgeInsets.only(left: 40, top: 20, right: 20),
                                                                              child: Column(children: [
                                                                                SizedBox(
                                                                                  height: 20,
                                                                                ),
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                  children: [
                                                                                    Text("Localisation", style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Nunito', fontSize: 15, color: Colors.black)),
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        Icon(
                                                                                          MdiIcons.send,
                                                                                          //color: color,
                                                                                          size: 10,
                                                                                        ),
                                                                                        SizedBox(
                                                                                          width: 5,
                                                                                        ),
                                                                                        Text(vehicules[index].distance.toString() + " mètres", style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Nunito', fontSize: 10, color: Colors.black))
                                                                                      ],
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                                SizedBox(
                                                                                  height: 10,
                                                                                ),
                                                                                Container(
                                                                                    decoration: BoxDecoration(border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(15)),
                                                                                    child: Container(
                                                                                      margin: EdgeInsets.all(10),
                                                                                      child: Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                        children: [
                                                                                          Icon(MdiIcons.marker
                                                                                              //color: color,
                                                                                              ),
                                                                                          SizedBox(
                                                                                            width: 5,
                                                                                          ),
                                                                                          Text(vehicules[index].location, style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.grey))
                                                                                        ],
                                                                                      ),
                                                                                    )),
                                                                                SizedBox(
                                                                                  height: 30,
                                                                                ),
                                                                                Column(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                                  children: [
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                                      children: [
                                                                                        Text("Informations sur le trajet ", style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Nunito', fontSize: 18, color: Colors.black)),
                                                                                      ],
                                                                                    ),
                                                                                    SizedBox(
                                                                                      height: 10,
                                                                                    ),
                                                                                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                                                                      Container(
                                                                                          decoration: BoxDecoration(border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(15)),
                                                                                          child: Container(
                                                                                            margin: EdgeInsets.all(10),
                                                                                            child: Column(
                                                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                              children: [
                                                                                                Text("Temps estimé  ", style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.black)),
                                                                                                SizedBox(
                                                                                                  height: 5,
                                                                                                ),
                                                                                                Text(vehicules[index].temps, style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.grey))
                                                                                              ],
                                                                                            ),
                                                                                          )),
                                                                                      SizedBox(
                                                                                        width: 30,
                                                                                      ),
                                                                                      Container(
                                                                                          decoration: BoxDecoration(border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(15)),
                                                                                          child: Container(
                                                                                            margin: EdgeInsets.all(10),
                                                                                            child: Column(
                                                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                              children: [
                                                                                                Text("Prix à payer  ", style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.black)),
                                                                                                SizedBox(
                                                                                                  height: 5,
                                                                                                ),
                                                                                                Text(vehicules[index].prix.toString() + ' ' + 'DA', style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.grey))
                                                                                              ],
                                                                                            ),
                                                                                          )),
                                                                                    ]),
                                                                                    SizedBox(
                                                                                      height: 20,
                                                                                    ),
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        Text("Méthode de paiement", style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Nunito', fontSize: 15, color: Colors.black)),
                                                                                      ],
                                                                                    ),
                                                                                    SizedBox(
                                                                                      height: 10,
                                                                                    ),
                                                                                    Container(
                                                                                        decoration: BoxDecoration(border: Border.all(color: Colors.black), borderRadius: BorderRadius.circular(15)),
                                                                                        child: Container(
                                                                                          margin: EdgeInsets.all(10),
                                                                                          child: Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                                            children: [
                                                                                              Text("xxxx-xxxx-xxxx-1234", style: TextStyle(fontFamily: 'Nunito', fontSize: 15, color: Colors.grey)),
                                                                                              SizedBox(
                                                                                                width: 5,
                                                                                              ),
                                                                                              Image.asset(
                                                                                                "images/BARIDIMOB.png",
                                                                                                width: 50,
                                                                                                height: 50,
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        )),
                                                                                  ],
                                                                                ),
                                                                                SizedBox(
                                                                                  height: 10,
                                                                                ),
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                                  children: [
                                                                                    SizedBox(
                                                                                      height: 10,
                                                                                    ),
                                                                                    Container(
                                                                                      alignment: Alignment.bottomCenter,
                                                                                      height: 130,
                                                                                      width: 300,
                                                                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.transparent),
                                                                                      child: Container(
                                                                                        alignment: Alignment.bottomLeft,
                                                                                        height: 50,
                                                                                        width: 250,
                                                                                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: d_blue),
                                                                                        child: MaterialButton(
                                                                                            onPressed: () {
                                                                                              Navigator.push(
                                                                                                context,
                                                                                                MaterialPageRoute(
                                                                                                  builder: (context) => InsideCar(),
                                                                                                ),
                                                                                              );
                                                                                            },
                                                                                            child: const Text(
                                                                                              "Cliquez pour déverrouiler",
                                                                                              textAlign: TextAlign.center,
                                                                                              style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: 'Nunito'),
                                                                                            )),
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ]),
                                                                            )))
                                                                  ],
                                                                ),
                                                              ));
                                                        });
                                                    ;
                                                  },
                                                  child: const Text(
                                                    "Louer",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontSize: 20,
                                                        color: Colors.white,
                                                        fontFamily: 'Nunito'),
                                                  )),
                                            ),
                                          ],
                                        )
                                      ]),
                                    )))
                          ],
                        ),
                      ));
                });
          },
          child: Stack(children: [
            Center(
                child: Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 20.0,
                    ),
                    height: 125.0,
                    width: 275.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: d_blue,
                            offset: Offset(0.0, 4.0),
                            blurRadius: 10.0,
                          ),
                        ]),

                    child: Container(

                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white),

                        child:Row(children: [
                    Container(
                    height: 70.0,
                        width: 90.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10.0),
                                topLeft: Radius.circular(10.0)),
                            image: DecorationImage(
                                image: AssetImage(vehicules[index].image),
                                fit: BoxFit.cover))),
          SizedBox(width: 8.0),
          Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  vehicules[index].marque,
                  style: TextStyle(
                      fontSize: 12.5,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  vehicules[index].location,
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w600),
                ),
                Container(
                  width: 170.0,
                  child: Text(
                    vehicules[index].matricule,
                    style: TextStyle(
                        fontSize: 11.0,
                        fontWeight: FontWeight.w300),
                  ),
                )
              ])
          ]))))
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(65),
          child: AppBar(
            backgroundColor: d_blue,
            leading: IconButton(
              iconSize: 30,
              icon: Icon(
                Icons.dehaze,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MenuPage(),
                  ),
                );
                ;
              },
            ),
            elevation: 3,
            titleSpacing: 0.0,
            title: Row(
              children: <Widget>[
                Text(
                  "  Commencez ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      letterSpacing: 1),
                ),
                Text(
                  'votre trajet! ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      letterSpacing: 1),
                ),
              ],
            ),
            actions: <Widget>[
              IconButton(
                iconSize: 30,
                icon: Icon(
                  Icons.notifications,
                ),
                onPressed: () {
                  ;
                },
              ),
            ],
          ),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 50.0,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                zoomGesturesEnabled: true,
                zoomControlsEnabled: false,
                compassEnabled: false,
                initialCameraPosition: CameraPosition(
                    target: LatLng(36.69742546502124, 3.075415949250581),
                    zoom: 12.0),
                markers: Set.from(allMarkers),
                onMapCreated: mapCreated,
              ),
            ),


            Positioned(
              //search input bar
                top: 90,
                right:20,
                child: InkWell(
                    onTap: () async {
                      var place = await PlacesAutocomplete.show(
                          context: context,
                          apiKey: googleApikey,
                          mode: Mode.overlay,
                          types: [],
                          strictbounds: false,
                          components: [Component(Component.country, 'dz')],
                          //google_map_webservice package
                          onError: (err) {
                            print(err);
                          });

                      if (place != null) {

                        setState(() {
                          location2 = place.description.toString();





                        });
                        //form google_maps_webservice package
                        final plist = GoogleMapsPlaces(
                          apiKey: googleApikey,
                          apiHeaders: await GoogleApiHeaders().getHeaders(),
                          //from google_api_headers package
                        );
                        String placeid = place.placeId ?? "0";
                        final detail = await plist.getDetailsByPlaceId(placeid);
                        final geometry = detail.result.geometry!;
                        final lat = geometry.location.lat;
                        final lang = geometry.location.lng;


                        var newlatlang = LatLng(lat, lang);

                        //move map camera to selected place with animation
                        _controller.animateCamera(
                            CameraUpdate.newCameraPosition(
                                CameraPosition(target: newlatlang, zoom: 17)));
                      }
                    },
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Card(
                        child: Container(
                            padding: EdgeInsets.all(0),
                            width: MediaQuery.of(context).size.width - 40,
                            child: ListTile(
                              title: Text(
                                location2,
                                style: TextStyle(fontSize: 18),
                              ),
                              trailing: Icon(Icons.search),
                              dense: true,
                            )),
                      ),
                    ))),
            Positioned(
              bottom: 0,
              child: Container(
                height: 150.0,
                width: MediaQuery.of(context).size.width,
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: vehicules.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _coffeeShopList(index);
                  },
                ),
              ),
            )
          ],
        ));
  }

  void mapCreated(controller) {
    setState(() {
      _controller = controller;
      locatePosition();
    });
  }

  Position? currentPosition;
  GoogleMapController? newGoogleMapController;
  void locatePosition() async {
    bool isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();

    await Geolocator.checkPermission();
    await Geolocator.requestPermission();

    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    currentPosition = position;
    LatLng latLngPosition = LatLng(position.latitude, position.longitude);
  }

  moveCamera() {
    _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: vehicules[_pageController.page!.toInt()].locationCoords,
        zoom: 14.0,
        bearing: 45.0,
        tilt: 45.0)));
  }
  Widget? findcars(int index)
  {


    if (vehicules[index].locationCoords.latitude==geometry.location.lat && vehicules[index].locationCoords.longitude==geometry.location.lng)
    {

      Row(children: [

        Container(
            height: 70.0,
            width: 90.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0)),
                image: DecorationImage(
                    image: AssetImage(vehicules[index].image),
                    fit: BoxFit.cover))),
        SizedBox(width: 8.0),
        Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                vehicules[index].marque,
                style: TextStyle(
                    fontSize: 12.5,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                vehicules[index].location,
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w600),
              ),
              Container(
                width: 170.0,
                child: Text(
                  vehicules[index].matricule,
                  style: TextStyle(
                      fontSize: 11.0,
                      fontWeight: FontWeight.w300),
                ),
              )
            ])
      ]);
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(
        DiagnosticsProperty<GoogleMapController>('_controller', _controller));
  }
}
